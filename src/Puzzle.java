import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Puzzle {

   /**
    * Solve the word puzzle.
    *
    * @param args three words (addend1, addend2 and sum)
    */
   public static void main(String[] args) {
      /*Puzzle puzzle = new Puzzle();
      String[] s = {"i", "b", "a"};
      puzzle.readInput(s); */
      Puzzle puzzle = new Puzzle();
      puzzle.readInput(args);
   }

   private String s1, s2, sum;
   private final ArrayList<Character> charset = new ArrayList<Character>();
   private final ArrayList<Boolean> booleans = new ArrayList<Boolean>();
   private final LinkedList<Link> links = new LinkedList<Link>();
   private final LinkedList<LinkedList<Link>> answer = new LinkedList<LinkedList<Link>>();

   private class Link{
      private final Character ch;
      private final Integer num;

      public Link(Character ch, Integer num) {
         this.ch = ch;
         this.num = num;
      }

      public Character getCh() {
         return ch;
      }

      public Integer getNum() {
         return num;
      }

      @Override
      public String toString() {
         return " (" + ch + " => " + num + ")";
      }
   }

   /**
    * Pre-process method created to read input. Also performs some of the first checks.
    * @param args arguments to be processed
    */
   private void readInput(String[] args) {
      if (args.length != 3) { throw new RuntimeException("Wrong number of arguments. Exactly 3 needed. You entered: "+ Arrays.toString(args)); }
      s1 = checkWord(args[0]);
      s2 = checkWord(args[1]);
      sum = checkWord(args[2]);
      init();
   }

   /**
    * Initialization method created to initialize necessary arrays.
    * Program also initializes first step in our recursive approach
    * to solve the problem.
    */
   private void init() {
      buildBooleans();
      solve(0);
      printAnswer();
   }

   /**
    * Checks a word. Validates it and formats it(all lowercase characters to uppercase)
    * @param arg argument to be checked
    * @return return validated and formatted word.
    */
   private String checkWord(String arg) {
      if (arg.length() > 18) { throw new RuntimeException("Argument exceeds limit of 18. Argument: "+arg); }
      buildCharset(arg.toUpperCase());
      return arg.toUpperCase();
   }

   /**
    * Method builds / generates a charset. Also checks if it stays within limits.
    * @param arg input String that is used to extend the charset
    */
   private void buildCharset(String arg) {
      for(Character ch: arg.toCharArray()) {
         if (!charset.contains(ch)) {
            charset.add(ch);
         }
         if (charset.size() > 10) { throw new RuntimeException("Too many unique letters. Maximum is 10."); }
      }
   }

   /**
    * Method to build booleans that indicate whether a number is already linked
    * to a digit or not.
    */
   private void buildBooleans() {
      for (int i = 0; i<10; i++) {booleans.add(false);}
   }

   /**
    * Main function to solve the problem.
    * We're using backtracking recursive approach.
    * @param index index that shows at which character we are in the charset
    */
   private void solve(int index) {
      if (allDigitsSet()) {
         if (check()) {
            answer.add((LinkedList<Link>)links.clone());
            return;
         }
         return;
      }
      if (index >= charset.size()) return;
      for (int i = 0; i<10; i++) {
         if (isFirstLetter(charset.get(index)) && i == 0) { continue; } //Skips i == 0, if current letter is first letter in any word.
         if (booleans.get(i).equals(false)) {
            booleans.set(i, true);
            links.addLast(new Link(charset.get(index), i));
            solve(index + 1);
            links.removeLast();
            booleans.set(i, false);
         }
      }
   }

   /**
    * Checks if all letters have numbers assigned.
    * @return true if yes, false if no
    */
   private boolean allDigitsSet() {
      return (links.size() == charset.size());
   }

   /**
    * Method checks if current links provide correct answer.
    * Equation: s1 + s2 = sum
    * @return true if equation is correct
    */
   private boolean check() { return (calc(s1) + calc(s2) == calc(sum)); }

   /**
    * Calculates value of string
    * @param arg String to be calculated
    * @return integer value of string
    */
   private int calc(String arg) {
      int sum=0;
      int j = arg.length();
      for (Character ch: arg.toCharArray()) {
         if (sum == 0 && findNumByLinkChar(ch) == 0) {return -1;}
         sum = sum + findNumByLinkChar(ch) * ((int)Math.pow(10, j-1));
         j--;
      }
      return sum;
   }

   /**
    * Finds an integer linked to specified character
    * @param ch search parameter(character)
    * @return integer value of character
    */
   private int findNumByLinkChar(Character ch) {
      for (Link item: links) {
         if (item.getCh().equals(ch)) {
            return item.getNum();
         }
      }
      return -1; //When nothing found
   }

   /**
    * Method prints out all found answers.
    */
   private void printAnswer() {
      int i = 1;
      System.out.println("\nPrinting total of "+ answer.size() + " answer(s).");
      for (LinkedList<Link> line: answer) {
         System.out.print("Answer[" + i + "]: ");
         for (Link item: line) {
            System.out.print(item);
         }
         i++;
         System.out.println(";");
      }
   }

   /**
    * Method checks if a character is first letter in any input word.
    * @param c character to be checked
    * @return true if it is first letter
    */
   private boolean isFirstLetter(Character c) {
      return (c.equals(s1.charAt(0)) ||
              c.equals(s2.charAt(0)) ||
              c.equals(sum.charAt(0)) );
   }
}